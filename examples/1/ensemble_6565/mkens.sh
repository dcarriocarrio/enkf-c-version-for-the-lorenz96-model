#!/bin/bash

set -eu

for var in temp salt u v
do
    echo -n $var
    for (( mem = 1; mem <= 96; ++mem ))
    do
	memstr=`printf "mem%03d" $mem`
	src=../../example/ensemble_6565.new/${memstr}_${var}.nc
	dst=${memstr}_${var}.nc
	if [ ! -f "$dst" ]
	then
	    ncks -d zaxis_1,0,27,27 $src $dst
	    echo -n "."
	else
	    echo -n "+"
	fi
    done
    echo
done
