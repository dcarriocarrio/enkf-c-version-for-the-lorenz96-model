#!/bin/bash

set -eu

for var in eta_t temp salt u v
do
    echo -n $var
    for (( mem = 1; mem <= 144; ++mem ))
    do
	memstr=`printf "mem%03d" $mem`
	src=../../example/ensemble_static.new/${memstr}_${var}.nc
	dst=${memstr}_${var}.nc
	if [ ! -f "$dst" ]
	then
	    ncks -d st_ocean,0,27,27 $src $dst
	    echo -n "."
	else
	    echo -n "+"
	fi
    done
    echo
done
